#include "mujoco.h"
#include "glfw3.h"

#include <ctime>
#include <cmath>
#include <chrono>
#include <iostream>

mjModel *m = nullptr;
mjData *d = nullptr;
mjvCamera cam;
mjvOption opt;
mjvScene scn;
mjrContext con;

bool button_left = false;
bool button_middle = false;
bool button_right = false;
double last_x = 0;
double last_y = 0;

void keyboard(GLFWwindow *window, int key, int scan_code, int act, int mods) {
    if (act == GLFW_PRESS && key == GLFW_KEY_BACKSPACE) {
        mj_resetData(m, d);
        mj_forward(m, d);
    }
}

void mouse_button(GLFWwindow *window, int button, int act, int mods) {
    button_left = (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS);
    button_middle = (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_MIDDLE) == GLFW_PRESS);
    button_right = (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS);

    glfwGetCursorPos(window, &last_x, &last_y);
}

void mouse_move(GLFWwindow *window, double xpos, double ypos) {
    if (!button_left && !button_middle && !button_right) {
        return;
    }

    double dx = xpos - last_x;
    double dy = ypos - last_y;
    last_x = xpos;
    last_y = ypos;

    int width, height;
    glfwGetWindowSize(window, &width, &height);

    bool mod_shift = (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS ||
                      glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS);

    mjtMouse action;
    if (button_right) {
        action = mod_shift ? mjMOUSE_MOVE_H : mjMOUSE_MOVE_V;
    } else if (button_left) {
        action = mod_shift ? mjMOUSE_ROTATE_H : mjMOUSE_ROTATE_V;
    } else {
        action = mjMOUSE_ZOOM;
    }

    mjv_moveCamera(m, action, dx / height, dy / height, &scn, &cam);
}

void scroll(GLFWwindow *window, double x_offset, double y_offset) {
    mjv_moveCamera(m, mjMOUSE_ZOOM, 0, -0.05 * y_offset, &scn, &cam);
}

void controller(const mjModel *m, mjData *d) {

    static auto last_time = d->time;
    static auto punched = false;
    mjtNum interval = 1.0;

    mjtNum hit[3] = { 15000000.0, 10000000.0, 1.0 };

    if (punched) {
        for (size_t i = 0; i < 3; i++) {
            d->qfrc_applied[i] -= hit[i];
        }
        punched = false;
    }

    auto time = d->time;
    if (time - last_time > interval) {
        last_time = time;

        for (size_t i = 0; i < 3; i++) {
            d->qfrc_applied[i] += hit[i];
        }

        punched = true;
    }
}

int main(int argc, char **argv) {
    mj_activate("mjkey.txt");

    std::srand(static_cast<unsigned int>(std::time(nullptr)));

    const char xml_path[] = "model/box.xml";
    char error[1000] = "Could not load binary model";

    m = mj_loadXML(xml_path, nullptr, error, 1000);
    if (!m) {
        mju_error_s("Load model error: %s", error);
    }

    d = mj_makeData(m);

    if (!glfwInit()) {
        mju_error("Could not initialize GLFW");
    }

    GLFWwindow *window = glfwCreateWindow(1200, 900, "Demo", nullptr, nullptr);
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);

    mjv_defaultCamera(&cam);
    mjv_defaultOption(&opt);
    mjv_defaultScene(&scn);
    mjr_defaultContext(&con);

    mjv_makeScene(m, &scn, 2000);
    mjr_makeContext(m, &con, mjFONTSCALE_150);

    glfwSetKeyCallback(window, keyboard);
    glfwSetCursorPosCallback(window, mouse_move);
    glfwSetMouseButtonCallback(window, mouse_button);
    glfwSetScrollCallback(window, scroll);

    mjcb_control = controller;

    while (!glfwWindowShouldClose(window)) {
        mjtNum sim_start = d->time;
        while (d->time - sim_start < 1.0 / 60.0) {
            mj_step(m, d);
        }

        mjrRect viewport = {0, 0, 0, 0};
        glfwGetFramebufferSize(window, &viewport.width, &viewport.height);

        mjv_updateScene(m, d, &opt, nullptr, &cam, mjCAT_ALL, &scn);
        mjr_render(viewport, &scn, &con);

        glfwSwapBuffers(window);

        glfwPollEvents();
    }

    mjv_freeScene(&scn);
    mjr_freeContext(&con);

    mj_deleteData(d);
    mj_deleteModel(m);
    mj_deactivate();

#if defined(__APPLE__) || defined(_WIN32)
    glfwTerminate();
#endif

    return 1;
}